unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Winapi.TlHelp32,
  MVC.DBSQLite, XSuperObject, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TForm1 = class(TForm)
    Button2: TButton;
    ds1: TDataSource;
    dbgrd1: TDBGrid;
    cds1: TFDQuery;
    dbnvgr1: TDBNavigator;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uDbConfig, uServer;

{$R *.dfm}

//  MVC 框架 WINForm 窗口 CS程序开发方式
procedure TForm1.Button2Click(Sender: TObject);
begin
  //服务层业务调用
  _Server.userServer.getall(cds1);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	//服务层及数据库层释放
  _Server.Free;
  Db.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
	//服务层及数据库层创建
  Db := TDBConfig.Create;
  _Server := TServer.Create(Db);
end;

end.

