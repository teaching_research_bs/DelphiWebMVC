unit userServer;

interface

uses
  XSuperObject, uTableMap, MVC.BaseService, System.SysUtils,FireDAC.Comp.Client;

type
  TUserServer = class(TBaseService)
  public
    function getlist(): ISuperObject;
    function getall(var cds:TFDQuery):Boolean;
  end;

implementation

{ TUserServer }
// 返回 数据集控件
function TUserServer.getall(var cds:TFDQuery): Boolean;
begin
  Db.Default.Find(tb_users,'',cds);
  Result:=true;
end;
// 返回 json格式数据
function TUserServer.getlist: ISuperObject;
begin
  Result := Db.Default.Find(tb_users, '');
end;

end.

// 要有 必须引用 uDbconfig.pas 单元，单元名不可修改。
//MVC框架 说明 可以同时开发 cs 与 bs 公共单元
// Server 模块 可以通用 ，可把数据库操作 部分封装成 server 模块，只要修改前端 即可完成 转换。
