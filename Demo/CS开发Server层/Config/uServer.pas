unit uServer;

interface

uses
  userServer, uDBConfig;

type
  TServer = class
  public
    // 创建具体的业务模块
    userServer: TUserServer;
    constructor Create(Db: TDBConfig);
    destructor Destroy; override;
  end;

var
  _Server: TServer;

implementation



{ TServer }

constructor TServer.Create(Db: TDBConfig);
begin
  userServer := TUserServer.Create(Db);

end;

destructor TServer.Destroy;
begin
  userServer.Free;
  inherited;
end;

end.

