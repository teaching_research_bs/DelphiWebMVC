program CS_Demo;

uses
  Vcl.Forms,
  Unit1 in 'View\Unit1.pas' {Form1},
  userServer in 'Server\userServer.pas',
  uServer in 'Config\uServer.pas',
  uDbConfig in 'Config\uDbConfig.pas',
  uTableMap in 'Config\uTableMap.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  application.Run;
end.
