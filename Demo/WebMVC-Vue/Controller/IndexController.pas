unit IndexController;

interface

uses
  MVC.BaseController, System.SysUtils, System.Classes, XSuperObject, uTableMap;

type
  TIndexController = class(TBaseController)
    procedure Index;
    procedure data;
    procedure check;
    procedure main;
  end;

implementation



{ TIndexController }

procedure TIndexController.check;
var
  username, pwd: string;
  json:string;
  map:ISuperObject;
begin
  with view do
  begin

    json := Request.Content;
    map:=SO(json);
    username := map['username'].AsString;
    pwd:=map['pwd'].AsString;
    if (username = 'admin') and (pwd = 'admin') then
      Success()
    else
      Fail(-1,'��¼ʧ��');
  end;
end;

procedure TIndexController.data;
var
  jo: ISuperObject;
begin
  with view do
  begin
    jo := SO();
    jo.S['title'] := '����ϵͳ';
    ShowJSON(jo);
  end;
end;

procedure TIndexController.Index;
begin
  with View do
  begin
    ShowHTML('index');
  end;
end;

procedure TIndexController.main;
begin
  with view do
  begin
    ShowHTML('main2');
  end;
end;

end.

