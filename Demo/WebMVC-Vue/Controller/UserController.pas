unit UserController;

interface

uses
  MVC.BaseController, System.SysUtils, System.Classes, XSuperObject, uTableMap;

type
  TUserController = class(TBaseController)
  public
  procedure index;
  end;

implementation

{ TUserController }

procedure TUserController.index;
begin
   with view do
   begin
     ShowHTML('index');
   end;
end;

initialization
  SetRoute('user',TUserController,'user',false);
end.

