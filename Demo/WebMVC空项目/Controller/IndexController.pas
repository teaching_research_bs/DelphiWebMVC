unit IndexController;

interface

uses
  MVC.BaseController, System.SysUtils, System.Classes, XSuperObject, uTableMap;

type
  TIndexController = class(TBaseController)
    procedure Index;
  end;

implementation



{ TIndexController }

procedure TIndexController.Index;
var
  name:string;
begin
  with View do
  begin
    name:=Input('username');//取get 或 post username参数值
    ShowHTML('index'); //输出index.html 模板
  end;
end;

end.
